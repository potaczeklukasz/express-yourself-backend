const express = require('express');
const checkAuth = require('../middleware/checkAuth');

const articleController = require('../controllers/articleController');

const router = express.Router();

// get newest articles
router.get('/newest/:page', articleController.getNewestArticles);

// get most popular articles
router.get('/popular/:page', articleController.getPopularArticles);

// get articles from category
router.get('/category/:category/:page', articleController.getArticlesFromCategory);

// get article by Id
router.get('/:articleId', articleController.getArticle);

// get article statistics
router.get('/:articleId/statistics', articleController.getArticleStatistics);

// get newest and most popular articles
router.get('/', articleController.getArticles);

// get all articles written by one user
router.get('/user/:userId', articleController.postUserArticlesById);

// add new article
router.post('/', checkAuth, articleController.postAddArticle);

// add comment to article
router.post('/:articleId/add/comment', checkAuth, articleController.postAddComment);

// get many articles by idArray
router.post('/many', articleController.postManyArticles);

// post current logged user articles
router.post('/user', checkAuth, articleController.postUserArticles);

// search articles
router.post('/search/:page', articleController.postSearchArticles);

// update article
router.patch('/:articleId', checkAuth, articleController.patchUpdateArticle);

// delete article
router.delete('/:articleId', checkAuth, articleController.deleteArticle);

module.exports = router;