const express = require('express');
const checkAuth = require('../middleware/checkAuth');

const categoryController = require('../controllers/categoryController');

const router = express.Router();

router.get('/', categoryController.getCategories);

router.post('/add', checkAuth, categoryController.postAddCategory);

router.delete('/:category', checkAuth, categoryController.deleteCategory);

module.exports = router;