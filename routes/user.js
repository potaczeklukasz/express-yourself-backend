const express = require('express');
const checkAuth = require('../middleware/checkAuth');

const userController = require('../controllers/userController');

const router = express.Router();

// register new user
router.post('/register', userController.postRegisterUser);

// get current user details
router.get('/', checkAuth, userController.getCurrentUserData);

// get user from id details
router.get('/:userId', userController.getUserData);

// post many users by id
router.post('/many', userController.postManyUsers);

// delete user
router.delete('/:userId', checkAuth, userController.deleteUser);

// update user details
router.patch('/:userId', checkAuth, userController.patchUser);

// update user favourite articles
router.patch('/:userId/favourite/articles', checkAuth, userController.patchArticles);

// update user favourite writers
router.patch('/:userId/favourite/writers', checkAuth, userController.patchWriters);

module.exports = router;