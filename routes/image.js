const express = require('express');
const checkAuth = require('../middleware/checkAuth');
const multer = require('multer');
const upload = multer();

const imageController = require('../controllers/imageController');

const router = express.Router();

// upload article image
router.post('/upload', checkAuth, upload.single("image"), imageController.postUploadArticleImage);

// send image back
router.get('/:fileName', imageController.getImage);

module.exports = router;