exports.get404 = (req, res, next) => {
	res.status(404).json({ error: true, description: "Page was not found" });
};
