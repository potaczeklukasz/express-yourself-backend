const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');

const { Article, saveArticleAndResponse, saveArticle } = require('../models/article');
const { Comment } = require('../models/comment');
const { Statistics } = require('../models/statistics');

const getNewestArticles = async (page, limit = 6) => {
	return await Article.paginate({}, {
		page: page,
		limit: limit,
		sort: { _id: -1 },
		select: { imagePath: 1, category: 1, title: 1 }
	});
}

const getPopularArticles = async (page, limit = 6) => {
	return await Article.paginate({}, {
		page: page,
		limit: limit,
		sort: { views: -1 },
		select: { imagePath: 1, category: 1, title: 1 }
	});
}

const sendArticles = async (res, articles) => {
	return res.status(200).json({
		error: false,
		description: "Articles successfully fetched",
		...articles
	});
}

exports.postAddArticle = async (req, res, next) => {
	const { authorId, imagePath, authorName, category, title, subtitle, text } = req.body;
	const dateCreated = new Date();
	const dateUpdated = dateCreated;

	const article = new Article({ authorId, imagePath, authorName, category, title, subtitle, text, dateCreated, dateUpdated });

	return saveArticleAndResponse(res, article, "The article has been successfully added for verification. We let you know when it's approved");
}

exports.getArticle = async (req, res, next) => {
	const { articleId } = req.params;
	const { gender, age } = req.query;

	try {
		const article = await Article.findById(articleId).exec();
		if (article) {
			const comments = await Comment.find({ articleId });

			if (gender && age) {
				const statistics = new Statistics({
					userAge: age,
					userGender: gender,
					readDate: new Date(),
					articleId: articleId
				});
				statistics.save();
			}

			res.status(200).json({ error: false, description: "Article successfully fetched", article: { ...article.toObject(), comments: comments } });
			article.views++;
			return saveArticle(article);
		} else {
			return res.status(404).json({ error: true, description: "Article not found" });
		}
	} catch (err) {
		return res.status(404).json({ error: true, description: "Article not found" });
	}
}

exports.getArticleStatistics = async (req, res, next) => {
	const { articleId } = req.params;

	try {
		const statistics = await Statistics.find({ articleId }).exec();
		if (statistics) {
			res.status(200).json({ error: false, description: "Statistics successfully fetched", statistics });
		} else {
			return res.status(404).json({ error: true, description: "Statistics not found" });
		}
	} catch (err) {
		return res.status(404).json({ error: true, description: "Statistics not found" });
	}
}

exports.patchUpdateArticle = async (req, res, next) => {
	const { articleId } = req.params;
	const dateUpdated = new Date();

	try {
		const article = await Article.findById(articleId).exec();

		Object.assign(article, { ...req.body, dateUpdated }); // update object with data from request

		return saveArticleAndResponse(res, article, "Article successfully updated");
	} catch (err) {
		return res.status(404).json({ error: true, description: "Article not found" });
	}
}

exports.deleteArticle = async (req, res, next) => {
	const { articleId } = req.params;

	try {
		const article = await Article.findByIdAndRemove(articleId).exec();
		console.log(path.join(__dirname, "..", "upload", article.imagePath));
		try {
			fs.unlinkSync(path.join(__dirname, "..", "upload", article.imagePath));
		} catch (err) {
			console.log(err);
		}

		return res.status(200).json({ error: false, description: "Article successfully deleted" });
	} catch (err) {
		return res.status(404).json({ error: true, description: "Article not found" });
	}
}

exports.getArticles = async (req, res, next) => {
	const popularArticles = await getPopularArticles(1);
	const newestArticles = await getNewestArticles(1);

	if (popularArticles.docs.length > 0) {
		sendArticles(res, {
			newestArticles: {
				currentPage: newestArticles.page,
				totalPages: newestArticles.totalPages,
				articles: newestArticles.docs
			},
			popularArticles: {
				currentPage: popularArticles.page,
				totalPages: popularArticles.totalPages,
				articles: popularArticles.docs
			}
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}

exports.postManyArticles = async (req, res, next) => {
	const { idArray } = req.body;

	const articles = await Article.find({ _id: { $in: idArray } }).select({ title: 1, imagePath: 1, category: 1 }).exec();

	if (articles.length > 0) {
		sendArticles(res, {
			articles: articles
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}

exports.postAddComment = async (req, res, next) => {
	const { articleId } = req.params;
	const { userName, userId, comment, userImage } = req.body;
	const dateCreated = new Date();
	const dateUpdated = dateCreated;

	const userComment = new Comment({ userName, userId, comment, dateCreated, dateUpdated, userImage, articleId });

	userComment.save();

	res.status(200).json({ error: false, description: "Comment successfully added" });
}

exports.postUserArticles = async (req, res, next) => {
	const { userId } = req.user;

	const articles = await Article.find({ authorId: userId }).exec();

	if (articles.length > 0) {
		sendArticles(res, {
			articles: articles
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}

exports.postUserArticlesById = async (req, res, next) => {
	const { userId } = req.params;

	const articles = await Article.find({ authorId: userId }).exec();

	if (articles.length > 0) {
		sendArticles(res, {
			articles: articles
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}

exports.getNewestArticles = async (req, res, next) => {
	const { page } = req.params;

	const newestArticles = await getNewestArticles(page);

	if (newestArticles.docs.length > 0) {
		sendArticles(res, {
			currentPage: newestArticles.page,
			totalPages: newestArticles.totalPages,
			newestArticles: newestArticles.docs
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}

exports.getPopularArticles = async (req, res, next) => {
	const { page } = req.params;

	const popularArticles = await getPopularArticles(page);

	if (popularArticles.docs.length > 0) {
		sendArticles(res, {
			currentPage: popularArticles.page,
			totalPages: popularArticles.totalPages,
			popularArticles: popularArticles.docs
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}

exports.getArticlesFromCategory = async (req, res, next) => {
	const { category, page } = req.params;
	const articles = await Article.paginate({ category: category }, { page: page, limit: 6, sort: { $natural: -1 } });

	if (articles.docs.length > 0) {
		sendArticles(res, {
			currentPage: articles.page,
			totalPages: articles.totalPages,
			articles: articles.docs
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}

exports.postSearchArticles = async (req, res, next) => {
	const { page } = req.params;
	const { searchText } = req.body;

	// make regex string to search in database
	const words = searchText.replace(/[^a-zA-Z ]/g, "").split(" ");

	let regex = "^(?=.*";

	words.forEach((word, i) => {
		if (i !== words.length - 1)
			regex += word + ")(?=.*";
		else
			regex += word + ").*$";
	});

	const articles = await Article.paginate(
		{
			$or: [
				{ title: { $regex: regex, $options: "i" } },
				{ text: { $regex: searchText, $options: "i" } }
			]
		},
		{
			page: page,
			limit: 6,
			sort: { $natural: -1 }
		});

	if (articles.docs.length > 0) {
		sendArticles(res, {
			currentPage: articles.page,
			totalPages: articles.totalPages,
			articles: articles.docs
		});
	} else {
		return res.status(404).json({ error: true, description: "Articles not found" });
	}
}