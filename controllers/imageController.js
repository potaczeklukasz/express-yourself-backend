const fs = require('fs');
const randomString = require('randomstring');
const sharp = require('sharp');
const sizeOf = require('image-size');
const path = require('path');

exports.postUploadArticleImage = async (req, res, next) => {
	const fileName = `${randomString.generate()}.${req.file.originalname.split('.')[1]}`;

	try {
		if (sizeOf(req.file.buffer).width > 1920) { // if width is bigger then 1920 - resize image
			try {
				sharp(req.file.buffer)
					.resize(1920)
					.toBuffer()
					.then(data => {
						fs.writeFile(path.join(__dirname, "..", "upload", fileName), data, () => {
							res.status(200).json({ error: false, description: "File successfully uploaded", fileName: fileName });
						});
					});
			} catch (err) {
				res.status(404).json({ error: true, description: "Uploading file failed" });
			}
		} else { // if not - save image with original size
			try {
				fs.writeFile(path.join(__dirname, "..", "upload", fileName), req.file.buffer, () => {
					res.status(200).json({ error: false, description: "File successfully uploaded", fileName: fileName });
				});
			} catch (err) {
				res.status(404).json({ error: true, description: "Uploading file failed" });
			}
		}
	} catch (err) {
		res.status(404).json({ error: true, description: "Uploading file failed" });
	}
}

exports.getImage = async (req, res, next) => {
	const { fileName } = req.params;
	res.sendFile(path.resolve(path.join("upload", fileName)));
}