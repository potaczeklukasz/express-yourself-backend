const { Category, saveCategory } = require('../models/category');

exports.getCategories = async (req, res, next) => {
	const categories = await Category.find({}).exec();

	if (!categories) return res.status(404).json({ error: true, description: "Categories not found" });

	res.status(200).json({ error: false, description: "Categories fetched successfully", categories: categories });
}

exports.postAddCategory = async (req, res, next) => {
	if (req.role !== "admin") return res.status(403).json({ error: true, description: "Only admin can add categories" });

	const category = new Category({ category: req.body.category });

	saveCategory(res, category, "Category was succesfully added");
}

exports.deleteCategory = async (req, res, next) => {
	if (req.role !== "admin") return res.status(403).json({ error: true, description: "Only admin can add categories" });

	const { category } = req.params;

	await Category.findOneAndDelete({ category }).exec();

	res.status(200).json({ error: false, description: "Category was successfully deleted" });
}