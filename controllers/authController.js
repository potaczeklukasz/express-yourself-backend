const { User } = require('../models/user');
const verifyPassword = require('../utils/password').verifyPassword;
const jwt = require('jsonwebtoken');
require('dotenv').config();

exports.postLogin = async (req, res, next) => {
	const { userName, password } = req.body;

	const user = await User.findOne({ userName }).exec();

	// check if user exist
	if (!user) return res.status(404).json({ error: true, description: "Username or password is incorrect" });

	// check if password is correct
	if (verifyPassword(password, user.password)) {
		// generate token
		const token = jwt.sign({ userName: user.userName, userId: user._id, role: user.role }, process.env.JWT_KEY, { expiresIn: '1h' });

		res.status(200).json({ error: false, description: "User successfully login", token: token, user: user });
	} else {
		res.status(404).json({ error: true, description: "Username or password is incorrect" });
	}
}