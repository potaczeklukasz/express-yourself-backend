const { User, getUser, saveUser } = require('../models/user');
const { Comment } = require('../models/comment');
const hashPassword = require('../utils/password').hashPassword;

const updateFavourite = async (req, res, callback) => {
	const { userId } = req.params;

	try {
		const user = await getUser(res, userId);
		if (user) callback(user);
	} catch (err) {
		res.status(409).json({ error: true, description: "User details cannot be updated" });
	}
}

exports.postRegisterUser = async (req, res, next) => {
	const { userName, email, firstName, lastName, sex, birthDate } = req.body;

	const existingUser = await User.findOne({ userName }).exec();

	if (existingUser) return res.status(409).json({ error: true, description: "User assigned to this username already exists" });

	const password = hashPassword(req.body.password);
	const dateCreated = new Date();
	const dateUpdated = dateCreated;
	const activeTime = 0;

	const user = new User({ userName, email, password, firstName, lastName, sex, birthDate, dateCreated, dateUpdated, activeTime });

	return saveUser(res, user, "User successfully registered");
}

exports.getCurrentUserData = async (req, res, next) => {
	const { userId } = req.user;

	try {
		const user = await getUser(res, userId);
		if (user) res.status(200).json({ error: false, description: "User data was successfully fetched", user: user });
	} catch (err) {
		res.status(404).json({ error: true, description: "User not found" });
	}
}

exports.getUserData = async (req, res, next) => {
	const { userId } = req.params;

	try {
		const user = await getUser(res, userId);
		if (user) res.status(200).json({ error: false, description: "User data was successfully fetched", user: user });
	} catch (err) {
		res.status(404).json({ error: true, description: "User not found" });
	}
}

exports.postManyUsers = async (req, res, next) => {
	const { idArray } = req.body;
	const users = await User.find({ _id: { $in: idArray } }).select({ firstName: 1, imagePath: 1, lastName: 1 }).exec();
	if (users.length > 0) res.status(200).json({ error: false, description: "Users data was successfully fetched", users: users });
	else res.status(404).json({ error: true, description: "Users not found" });
}

exports.deleteUser = async (req, res, next) => {
	const { userId } = req.params;

	const user = await User.findByIdAndDelete(userId);

	if (!user) return res.status(404).json({ error: true, description: "User not found" });

	res.status(200).json({ error: false, description: "User was successfully deleted" });
}

exports.patchUser = async (req, res, next) => {
	const { userId } = req.params;
	const updatedData = { ...req.body, dateUpdated: new Date() }
	let user = null;

	try {
		user = await User.findByIdAndUpdate(userId, updatedData).exec();

		if (!user) return res.status(404).json({ error: true, description: "User not found" });

		res.status(204).json({ error: false, description: "Details successfully updated" });
	} catch (err) {
		res.status(409).json({ error: true, description: "User details cannot be updated" });
	}

	await Comment.updateMany({ userId }, {
		userName: `${req.body.firstName} ${req.body.lastName}`,
		userImage: req.body.imagePath
	});
}

exports.patchArticles = async (req, res, next) => {
	const { favouriteArticles } = req.body;

	updateFavourite(req, res, (user) => {
		user.favouriteArticles = favouriteArticles;
		return saveUser(res, user, "User details successfully updated");
	});
}

exports.patchWriters = async (req, res, next) => {
	const { favouriteWriters } = req.body;

	updateFavourite(req, res, (user) => {
		user.favouriteWriters = favouriteWriters;
		return saveUser(res, user, "User details successfully updated");
	});
}