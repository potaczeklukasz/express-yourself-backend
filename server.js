const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();

const errorController = require('./controllers/errorController');
const userRoutes = require('./routes/user');
const authRoutes = require('./routes/auth');
const categoryRoutes = require('./routes/category');
const articleRoutes = require('./routes/article');
const imageRoutes = require('./routes/image');

const PORT = process.env.PORT || 3001;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// ROUTES
app.use('/api/users', userRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/category', categoryRoutes);
app.use('/api/article', articleRoutes);
app.use('/api/image', imageRoutes);

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build'));

	const path = require('path');
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

app.use(errorController.get404);

mongoose
	.connect(`mongodb://${process.env.DATABASE_USER}:${process.env.DATABASE_PASSWORD}@ds255463.mlab.com:55463/express-yourself`, { useNewUrlParser: true })
	.then(result => {
		app.listen(PORT, "0.0.0.0", () => {
			console.log(`Server is running on PORT: ${PORT}`);
		});
	})
	.catch(err => {
		console.log(err);
	});

mongoose.set('useFindAndModify', false);