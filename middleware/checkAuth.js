const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = (req, res, next) => {
	if (req.headers.authorization) {
		const token = req.headers.authorization.split(" ")[1];

		try {
			const decoded = jwt.verify(token, process.env.JWT_KEY, null);
			req.user = decoded;

			next();
		} catch (err) {
			res.status(401).json({ error: true, description: "Authentication failed" });
		}
	} else {
		res.status(401).json({ error: true, description: "Request didn't contain the Authorization header" });
	}
}