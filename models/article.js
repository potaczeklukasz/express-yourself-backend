const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const statisticsSchema = require('./statistics');
const { commentSchema } = require('./comment');

const Schema = mongoose.Schema;

const articleSchema = new Schema({
	authorId: {
		type: String,
		required: true
	},
	imagePath: String,
	authorName: {
		type: String,
		required: true
	},
	category: {
		type: String,
		required: true
	},
	title: {
		type: String,
		required: true
	},
	subtitle: String,
	text: {
		type: String,
		required: true
	},
	status: {
		type: String,
		default: "to verification"
	},
	views: {
		type: Number,
		default: 0
	},
	dateCreated: {
		type: Date,
		required: true
	},
	dateUpdated: {
		type: Date,
		required: true
	}
});

articleSchema.plugin(mongoosePaginate); // add mongoose-pagination

const Article = mongoose.model('Article', articleSchema);

exports.saveArticleAndResponse = (res, article, description) => {
	article.save()
		.then(result => {
			res.status(201).json({ error: false, description: description });
		})
		.catch(err => {
			res.status(409).json({ error: true, description: err.message });
		});
}

exports.saveArticle = (article) => {
	article.save()
		.catch(err => {
			console.log(err);
		});
}

exports.Article = Article;