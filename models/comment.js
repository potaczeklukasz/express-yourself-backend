const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const commentSchema = new Schema({
	userName: {
		type: String,
		required: true
	},
	userImage: {
		type: String,
		default: 'user.png'
	},
	userId: {
		type: String,
		required: true
	},
	comment: {
		type: String,
		required: true
	},
	dateCreated: {
		type: Date,
		required: true
	},
	dateUpdated: {
		type: Date,
		required: true
	},
	edited: {
		type: Boolean,
		default: false
	},
	articleId: {
		type: String,
		required: true
	}
});

const Comment = mongoose.model('Comment', commentSchema);

exports.Comment = Comment;