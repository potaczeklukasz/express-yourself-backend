const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categorySchema = new Schema({
	category: {
		type: String,
		required: true
	},
	articlesAmount: {
		type: Number,
		default: 0
	}
});

const Category = mongoose.model('Category', categorySchema);

exports.saveCategory = (res, category, description) => {
	category.save()
		.then(result => {
			res.status(201).json({ error: false, description: description });
		})
		.catch(err => {
			res.status(409).json({ error: true, description: err.message });
		});
}

exports.Category = Category;