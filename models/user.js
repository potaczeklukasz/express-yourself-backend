const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
	userName: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	imagePath: {
		type: String,
		default: 'user.png'
	},
	password: {
		type: String,
		required: true
	},
	role: {
		type: String,
		default: "user"
	},
	firstName: String,
	lastName: String,
	sex: String,
	birthDate: {
		type: Date,
		required: true
	},
	dateCreated: {
		type: Date,
		required: true
	},
	dateUpdated: {
		type: Date,
		required: true
	},
	favouriteArticles: {
		type: Array,
		required: true
	},
	favouriteWriters: {
		type: Array,
		required: true
	}
});

const User = mongoose.model('User', userSchema)

exports.getUser = async (res, userId) => {
	const user = await User.findById(userId);
	if (!user) {
		res.status(404).json({ error: true, description: "User not found" });
		return false;
	}
	return user;
}

exports.saveUser = (res, user, description) => {
	user.save()
		.then(result => {
			res.status(201).json({ error: false, description: description });
		})
		.catch(err => {
			res.status(409).json({ error: true, description: err.message });
		});
}

exports.User = User;