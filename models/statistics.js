const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const statisticsSchema = new Schema({
	userAge: String,
	userGender: String,
	readDate: Date,
	articleId: String
});

const Statistics = mongoose.model('Statistics', statisticsSchema);

exports.Statistics = Statistics;