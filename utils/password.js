const passwordHash = require('password-hash');

exports.hashPassword = (password) => {
	const hashedPassword = passwordHash.generate(password);
	return hashedPassword;
}

exports.verifyPassword = (password, hashedPassword) => {
	return passwordHash.verify(password, hashedPassword);
}